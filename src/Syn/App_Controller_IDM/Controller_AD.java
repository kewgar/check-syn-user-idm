package Syn.App_Controller_IDM;

import Syn.File.ReadProperties;
import Syn.Endpoint.AD;
import static Syn.Endpoint.AD.connectAD;
import Syn.Endpoint.LDAP;
import static Syn.Model.Model_Data.UserlistAD;
import static Syn.Model.Model_Data.*;
import java.io.IOException;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

public class Controller_AD {

//    public static void main(String[] args) throws NamingException, IOException {
//        Controller_AD();
//        for (int i = 0; i < UserlistAD.size(); i++) {
//           
//                System.out.println(UserlistAD.get(i).get("eTSubord"));
//                System.err.println(UserlistAD.get(i).get("eTSuper"));
//                System.out.println(UserlistAD.get(i).get("eTCID"));
//            
//
//        }
//    }

    public static void Controller_AD() throws IOException, NamingException {
//        ReadProperties.SetProvisioningInformation();
        LdapContext ctxad = connectAD();
        UserlistAD = new ArrayList<>();
        UserlistAD = AD.ReadUserFromAD(ctxad);
        ctxad.close();

    }
}
