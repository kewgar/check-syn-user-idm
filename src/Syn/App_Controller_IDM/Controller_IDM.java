package Syn.App_Controller_IDM;

import Syn.File.ReadProperties;
import Syn.IDM.IDM;
import static Syn.IDM.IDM.connectIDM;
import Syn.Method.TestConection;
import static Syn.Model.Model_Data.UserlistIDM;
import java.io.IOException;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

public class Controller_IDM {

//    public static void main(String[] args) throws NamingException, IOException {
//        Controller_IDM();
//        for (int i = 0; i < UserlistIDM.size(); i++) {
//            System.out.println(UserlistIDM.get(i));
//        }
//    }

    public static void Controller_IDM() throws NamingException {
//        ReadProperties.SetProvisioningInformation();
        TestConection.Select_Server_Provisioning();
        LdapContext ctxidm = connectIDM();
        UserlistIDM = new ArrayList<>();
        UserlistIDM = IDM.ReadUserFromIDM(ctxidm);
        ctxidm.close();
    }

}
