package Syn.App_Controller_IDM;

import Syn.File.ReadProperties;

import Syn.Endpoint.LDAP;
import static Syn.Endpoint.LDAP.connectLDAP;
import static Syn.Model.Model_Data.*;
import java.io.IOException;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

public class Controller_LDAP {

    public static void main(String[] args) throws NamingException, IOException {

//        Controller_LDPA();
//        for (int i = 0; i < UserlistLDAP.size(); i++) {
//            System.out.println(UserlistLDAP.get(i).get("eTSubordFulluid"));
//            System.err.println(UserlistLDAP.get(i).get("eTSuper"));
//            System.out.println(UserlistLDAP.get(i).get("eTCID"));
//        }

    }

    public static void Controller_LDPA() throws IOException, NamingException {
//        ReadProperties.SetProvisioningInformation();
        LdapContext ctxad = connectLDAP();
        UserlistLDAP = new ArrayList<>();
        UserlistLDAP = LDAP.ReadUserFromLDAP(ctxad);
        ctxad.close();

    }
}
