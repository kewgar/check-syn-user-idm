package Syn.DB;

import static Syn.Model.Model_Constant.deleteDormantDB;
import static Syn.Model.Model_Constant.insertDormant_Provisioning_Account;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import static Syn.Model.Model_Data.CONNECTION_STRING;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class Method_DB extends Syn.Model.Model_Data {

    public static String connect() {
        String url = CONNECTION_STRING;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
            if (conn != null) {
//                logger.info("[Step Database 5.1] Connect DB Success");
                return url;
            } else {
                return null;
            }
        } catch (SQLException e) {
//            logger.error("[Step Database 5.1] Connect DB Fail" + e.getMessage());
        }
        return null;
    }

    public static void Delete_DB_Daily(String con) {
        String sql = deleteDormantDB;
        try (Connection cona = DriverManager.getConnection(con);
                PreparedStatement pstmt = cona.prepareStatement(sql)) {
            pstmt.executeUpdate();
//            logger.info("[Step Database 5.4] Delete DB_Daily Success");
            System.err.println("Clear DB");
        } catch (SQLException e) {
//            logger.error("[Step Database 5.4] Delete DB_Daily Fail" + e.getMessage());
        }
    }

    public static void InsertDataDB_Daily(String con, String endpointuid, String endpointou, String uid, String imEmployeeStatus,
            String imString03, String imString52, String imString53, String imLoginId, String imExpirationDate, String imEnabledState) {

        String sql = insertDormant_Provisioning_Account;
        try (Connection cona = DriverManager.getConnection(con);
                PreparedStatement pstmt = cona.prepareStatement(sql)) {
            pstmt.setString(1, uid);
            pstmt.setString(2, endpointuid);
            pstmt.setString(3, endpointou);
            pstmt.setString(4, imEmployeeStatus);
            pstmt.setString(5, imString03);
            pstmt.setString(6, imString52);
            pstmt.setString(7, imString53);
            pstmt.setString(8, imLoginId);
            pstmt.setString(9, imExpirationDate);
            pstmt.setString(10, imEnabledState);

            pstmt.executeUpdate();
//            logger.info("Insert DB success " + Shortname + "," + Status + "," + Endpoint + "," + ModifyTimestamp + "," + LastSignOn + "," + accountName);
        } catch (SQLException e) {
//            logger.error("[Step Database 5.4] insertData DB_Daily Fail" + e.getMessage());
            System.err.println(e);
        }
    }

////////////////////////////////////////////////////////////////////////    


    public static ArrayList<HashMap<String, String>> Read_StatusUserinAccountActivity(String con, String endPoint, String uid) {
        ArrayList<HashMap<String, String>> Userlist = new ArrayList<>();
        HashMap<String, String> map1;
        String action = "Dormant";
        try (Connection cona = DriverManager.getConnection(con); Statement stmt = cona.createStatement();) {
            String SQL = "SELECT DATE, PersonID, AccountName, Endpoint, [Action] as Action, RoleName "
                    + "FROM CA_IDG.dbo.IDM_AccountActivity where Endpoint like '" + endPoint + "' "
                    + "and DATE = "
                    + "(SELECT TOP 1 Date FROM CA_IDG.dbo.IDM_AccountActivity "
                    + "where PersonID like '" + uid + "' and CAST([Date] as date) != CAST(getdate() as date) order by Date DESC) "
                    + "and PersonID = "
                    + "(SELECT TOP 1 PersonID FROM CA_IDG.dbo.IDM_AccountActivity "
                    + "where PersonID like '" + uid + "' and CAST([Date] as date) != CAST(getdate() as date) order by Date DESC) "
                    + "and Action like '%" + action + "%'";
            ResultSet rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                map1 = new HashMap<>();
                map1.put("uid", rs.getString("PersonID"));
                map1.put("accountName", rs.getString("AccountName"));
                map1.put("status", rs.getString("Action"));
                Userlist.add(map1);
            }

            return Userlist;
        } catch (SQLException e) {
            e.printStackTrace();
//            logger.error("[Step Database 5.3] Read UID and Endpoint DB Fail" + e.getMessage());
        }
        return null;
    }

}
