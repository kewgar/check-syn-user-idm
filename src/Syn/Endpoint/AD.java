package Syn.Endpoint;

import Syn.File.ReadProperties;
import static Syn.Method.ETL.splittext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import static Syn.Model.Model_Data.*;

public class AD extends Syn.Model.Model_Data {

    public static void main(String[] args) {
//        ReadProperties.SetProvisioningInformation();
        LdapContext ctxidm = connectAD();
        UserlistAD = new ArrayList<>();
        UserlistAD = ReadUserFromAD(ctxidm);

        for (int i = 0; i < UserlistAD.size(); i++) {
            System.out.println(UserlistAD.get(i).get("eTSuper"));
//            System.err.println(UserlistAD.get(i).get("eTSubordou"));
        }
    }

    public static LdapContext connectAD() {
        try {
            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY_all);
            env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION_all);

            env.put(Context.SECURITY_PRINCIPAL, SECURITY_PRINCIPAL_AD);
            env.put(Context.SECURITY_CREDENTIALS, SECURITY_CREDENTIALS_AD);
            env.put(Context.PROVIDER_URL, PROVIDER_URL_AD);
            LdapContext ctx = new InitialLdapContext(env, null);
            System.out.println("con ad");
//            logger.info("[Step IDM 1.1] Connection Successful.");
            return ctx;
        } catch (NamingException e) {
//            logger.error("[Step IDM 1.1] Connection Fail." + e);
            return null;
        }
    }

    public static ArrayList<HashMap<String, String>> ReadUserFromAD(LdapContext ctx) {

        SearchControls searchCtls = new SearchControls();
        String returnedAtts[] = returnedAtts_AD;
        searchCtls.setReturningAttributes(returnedAtts);
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchFilter = searchFilter_AD;
        String searchBase = searchBase_AD;
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        HashMap<String, String> map1;

        try {

            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);

            int i = 0;
            while (answer != null && answer.hasMoreElements()) {
                i++;
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                map1 = new HashMap<>();
               
                if (attrs.get("eTSubordinateClassEntry") != null && !attrs.get("eTSubordinateClassEntry").equals("")) {
                    map1.put("eTSubord", splittext(attrs.get("eTSubordinateClassEntry").toString().trim()));
//                    System.out.println(attrs.get("eTSubordinateClassEntry").toString().trim());
                    String uid = splittext(attrs.get("eTSubordinateClassEntry").toString()).split(",")[0];
                    String ou = splittext(attrs.get("eTSubordinateClassEntry").toString()).split(",")[1].replaceFirst(":", "");

                    map1.put("eTSuborduid", uid);
                    map1.put("eTSubordou", ou);
                }
                if (attrs.get("eTSuperiorClassEntry") != null && !attrs.get("eTSuperiorClassEntry").equals("")) {
                    // map1.put("eTSuper", splittext(attrs.get("eTSuperiorClassEntry").toString().trim()));

                    String uidGlobal = splittext(attrs.get("eTSuperiorClassEntry").toString().split(",")[0]);
                    map1.put("eTSuper", uidGlobal);

//                    System.out.println(attrs.get("eTSuperiorClassEntry").toString().trim());
                }

                if (attrs.get("eTCID") != null && !attrs.get("eTCID").equals("")) {
                    // map1.put("eTSuper", splittext(attrs.get("eTSuperiorClassEntry").toString().trim()));

                    String uidGlobal = (attrs.get("eTCID").toString());
                    map1.put("eTCID", uidGlobal);

//                    System.out.println(attrs.get("eTSuperiorClassEntry").toString().trim());
                }

                userList.add(map1);
//                if (i==5) {
//                    break;
//                }
            }

//            logger.info("[Step IDM 1.2] Read users succes : " + userList.size());
            return userList;

        } catch (Exception e) {

            System.err.println(e + "Maybe connection Provisining Store has close");
//            logger.error("[Step IDM 1.2] Read Users Fail : " + e);
            return null;
        }

    }

}
