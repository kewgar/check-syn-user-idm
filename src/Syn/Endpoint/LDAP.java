package Syn.Endpoint;

import static Syn.Method.ETL.splittext;
import static Syn.Model.Model_Data.*;
import static Syn.Model.Model_Data.returnedAtts_AD;
import static Syn.Model.Model_Data.searchBase_AD;
import static Syn.Model.Model_Data.searchFilter_AD;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

/**
 *
 * @author User
 */
public class LDAP {

    public static LdapContext connectLDAP() {
        try {
            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY_all);
            env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION_all);

            env.put(Context.SECURITY_PRINCIPAL, SECURITY_PRINCIPAL_LDAP);
            env.put(Context.SECURITY_CREDENTIALS, SECURITY_CREDENTIALS_LDAP);
            env.put(Context.PROVIDER_URL, PROVIDER_URL_LDAP);
            LdapContext ctx = new InitialLdapContext(env, null);
            System.out.println("con ldap");
//            logger.info("[Step IDM 1.1] Connection Successful.");
            return ctx;
        } catch (NamingException e) {
//            logger.error("[Step IDM 1.1] Connection Fail." + e);
            return null;
        }
    }

    public static ArrayList<HashMap<String, String>> ReadUserFromLDAP(LdapContext ctx) {

        SearchControls searchCtls = new SearchControls();
        String returnedAtts[] = returnedAtts_LDAP;
        searchCtls.setReturningAttributes(returnedAtts);
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchFilter = searchFilter_LDAP;
        String searchBase = searchBase_LDAP;
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        HashMap<String, String> map1;

        try {

            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
            int i = 0;
            while (answer != null && answer.hasMoreElements()) {
                i++;
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                map1 = new HashMap<>();
//                 System.out.println(attrs);
                if (attrs.get("eTSubordinateClassEntry") != null && !attrs.get("eTSubordinateClassEntry").equals("")) {
//                    map1.put("eTSubord", splittext(attrs.get("eTSubordinateClassEntry").toString().trim()));
//                    System.out.println(attrs.get("eTSubordinateClassEntry").toString().trim());

                    String uid = splittext(attrs.get("eTSubordinateClassEntry").toString()).split(",")[0];
                    String ou = splittext(attrs.get("eTSubordinateClassEntry").toString()).split(",")[1];

                    map1.put("eTSubordFulluid", uid);
                    map1.put("eTSuborduid", uid.substring(2, uid.length()));
                    map1.put("eTSubordou", ou);

                }
                if (attrs.get("eTSuperiorClassEntry") != null && !attrs.get("eTSuperiorClassEntry").equals("")) {
//                    map1.put("eTSuper", splittext(attrs.get("eTSuperiorClassEntry").toString().trim()));
//                    System.out.println(attrs.get("eTSuperiorClassEntry").toString().trim());

                    String uidGlobal = splittext(attrs.get("eTSuperiorClassEntry").toString().split(",")[0]);
                    map1.put("eTSuper", uidGlobal);
                }

//                if (i == 5) {
//                    break;
//                }
                if (attrs.get("eTCID") != null && !attrs.get("eTCID").equals("")) {
                    // map1.put("eTSuper", splittext(attrs.get("eTSuperiorClassEntry").toString().trim()));

                    String uidGlobal = (attrs.get("eTCID").toString());
                    map1.put("eTCID", uidGlobal);

//                    System.out.println(attrs.get("eTSuperiorClassEntry").toString().trim());
                }
                userList.add(map1);
            }

//            logger.info("[Step IDM 1.2] Read users succes : " + userList.size());
            return userList;

        } catch (Exception e) {
            System.err.println("WTH Is Fail");
//            logger.error("[Step IDM 1.2] Read Users Fail : " + e);
            return null;
        }

    }

}
