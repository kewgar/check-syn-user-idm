package Syn.File;

import Syn.Endpoint.AD;
import static Syn.Endpoint.AD.connectAD;
import Syn.Mapping.Mapping;
import static Syn.Mapping.Mapping.Mappinguser;
import static Syn.Model.Model_Data.UserlistAD;

import static Syn.Model.Model_Data.listOutput;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;

public class Output {

//    public static void main(String[] args) throws IOException, NamingException {
//        ReadProperties.SetProvisioningInformation();
//
//        listOutput = new ArrayList<>();
//        listOutput = Mappinguser();
//
//        System.err.println(listOutput);
//        for (int i = 0; i < 10; i++) {
//            System.out.println(listOutput.get(i));
//        }
//    }
    public static void Controller_map(String endpoint) throws IOException, NamingException {
//        ReadProperties.SetProvisioningInformation();
        Mapping map = new Mapping();
        listOutput = new ArrayList<>();
        listOutput = map.Mappinguser(endpoint);

    }

    public static void writeCSVfile(String outputpath) throws IOException, NamingException {
       
        try {

            CSVWriter writer = new CSVWriter(new FileWriter(outputpath), ',', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
            List<String[]> ls = new ArrayList<String[]>();
            ls.add(new String[]{
                "uid",
                "uid_endpoint",
                "ou_endpoint",
                "imEmployeeStatus",
                "imString03",
                "imString52",
                "imString53",
                "imLoginId",
                "imExpirationDate",
                "imEnabledState",
                "Endpoint",});

            for (int i = 0; i < listOutput.size(); i++) {

                ls.add(new String[]{
                    '"' + listOutput.get(i).get("uid") + '"',
                    '"' + listOutput.get(i).get("userid") + '"',
                    '"' + listOutput.get(i).get("userou") + '"',
                    '"' + listOutput.get(i).get("imEmployeeStatus") + '"',
                    '"' + listOutput.get(i).get("imString03") + '"',
                    '"' + listOutput.get(i).get("imString52") + '"',
                    '"' + listOutput.get(i).get("imString53") + '"',
                    '"' + listOutput.get(i).get("imLoginId") + '"',
                    '"' + listOutput.get(i).get("imExpirationDate") + '"',
                    '"' + listOutput.get(i).get("imEnabledState") + '"',
                    '"' + listOutput.get(i).get("Endpoint") + '"'
                });
            }
            //logger.info("[Step 3.1] Duplicate Users : " + totaluserdup + " and Dormant Users : " + totaluserdormant);
            writer.writeAll(ls);
            writer.close();
        } catch (Exception a) {
//            logger.error("[Step 3.1] Write file fail : " + a);

        }
    }
}
