package Syn.File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class ReadProperties extends Syn.Model.Model_Data {

//    public static void main(String[] args) {
//        ReadProperties.SetProvisioningInformation();
//        System.err.println(INITIAL_CONTEXT_FACTORY_all + " " + SECURITY_AUTHENTICATION_all);
//    }

    public static void SetProvisioningInformation(String path) {
        Properties prop = new Properties();
        InputStream input = null;

        try {

            input = new FileInputStream(path);

            prop.load(input);

            //String 
            INITIAL_CONTEXT_FACTORY_all = (prop.getProperty("INITIAL_CONTEXT_FACTORY_all"));
            SECURITY_AUTHENTICATION_all = (prop.getProperty("SECURITY_AUTHENTICATION_all"));

            //AD
            SECURITY_PRINCIPAL_AD = (prop.getProperty("SECURITY_PRINCIPAL_AD"));
            SECURITY_CREDENTIALS_AD = (prop.getProperty("SECURITY_CREDENTIALS_AD"));
            PROVIDER_URL_AD = (prop.getProperty("PROVIDER_URL_AD"));
            searchFilter_AD = (prop.getProperty("searchFilter_AD"));
            searchBase_AD = (prop.getProperty("searchBase_AD"));

            //LDAP
            SECURITY_PRINCIPAL_LDAP = (prop.getProperty("SECURITY_PRINCIPAL_LDAP"));
            SECURITY_CREDENTIALS_LDAP = (prop.getProperty("SECURITY_CREDENTIALS_LDAP"));
            PROVIDER_URL_LDAP = (prop.getProperty("PROVIDER_URL_LDAP"));
            searchFilter_LDAP = (prop.getProperty("searchFilter_LDAP"));
            searchBase_LDAP = (prop.getProperty("searchBase_LDAP"));

            //IDM
            SECURITY_PRINCIPAL_IDM = (prop.getProperty("SECURITY_PRINCIPAL_IDM"));
            SECURITY_CREDENTIALS_IDM = (prop.getProperty("SECURITY_CREDENTIALS_IDM"));
            PROVIDER_URL_IDM_1 = (prop.getProperty("PROVIDER_URL_IDM_1"));
            PROVIDER_URL_IDM_2 = (prop.getProperty("PROVIDER_URL_IDM_2"));
            searchFilter_IDM = (prop.getProperty("searchFilter_IDM"));
            searchBase_IDM = (prop.getProperty("searchBase_IDM"));
            idm_enviroment = (prop.getProperty("idm_enviroment"));
            idm_server1 = (prop.getProperty("idm_server1"));
            idm_server2 = (prop.getProperty("idm_server2"));

            //part
            log_path = (prop.getProperty("log_path"));
            output_path = (prop.getProperty("output_path"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
