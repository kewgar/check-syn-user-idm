package Syn.IDM;

import static Syn.Method.ETL.decode;
import static Syn.Model.Model_Data.INITIAL_CONTEXT_FACTORY_all;
import static Syn.Model.Model_Data.PROVIDER_URL_IDM;
import static Syn.Model.Model_Data.SECURITY_AUTHENTICATION_all;
import static Syn.Model.Model_Data.SECURITY_CREDENTIALS_IDM;
import static Syn.Model.Model_Data.SECURITY_PRINCIPAL_IDM;
import static Syn.Model.Model_Data.returnedAtts_IDM;
import static Syn.Model.Model_Data.searchBase_IDM;
import static Syn.Model.Model_Data.searchFilter_IDM;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class IDM extends Syn.Model.Model_Data {

    public static LdapContext connectIDM() {
        try {

            Hashtable env = new Hashtable();
            env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY_all);
            env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION_all);
            env.put(Context.SECURITY_PRINCIPAL, SECURITY_PRINCIPAL_IDM);
            env.put(Context.SECURITY_CREDENTIALS, decode(SECURITY_CREDENTIALS_IDM));
            env.put(Context.PROVIDER_URL, PROVIDER_URL_IDM);
            LdapContext ctx = new InitialLdapContext(env, null);
            System.out.println("IDM Connection Successful");
//            logger.info("[Step IDM 1.1] Connection Successful.");
            System.out.println("Connecting IDM");
            return ctx;

        } catch (NamingException e) {
//            logger.error("[Step IDM 1.1] Connection Fail." + e);
            System.out.println(e);
            return null;
        }

    }

    public static ArrayList<HashMap<String, String>> ReadUserFromIDM(LdapContext ctx) {

        SearchControls searchCtls = new SearchControls();
        String returnedAtts[] = returnedAtts_IDM;
        searchCtls.setReturningAttributes(returnedAtts);
        searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String searchFilter = searchFilter_IDM;
        String searchBase = searchBase_IDM;
        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        HashMap<String, String> map1;
        int i = 0;
        try {
            NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter, searchCtls);
            while (answer != null && answer.hasMoreElements()) {
                SearchResult sr = (SearchResult) answer.next();
                Attributes attrs = sr.getAttributes();
                map1 = new HashMap<>();
                if ( //1 User ID (uid)
                        attrs.get("uid") != null && !attrs.get("uid").equals("")) {
                    map1.put("uid", (attrs.get("uid").toString().replaceFirst("uid: ", "")).trim());

                    //2 Login ID (imLoginId)
                    map1.put("imLoginId", (attrs.get("imLoginId").toString().replaceFirst("imLoginId: ", "")).trim());

                    //3 Full Employee ID (imString03)
                    String subID = "";

                    if (attrs.get("imString03") != null && !attrs.get("imString03").equals("")) {
                        map1.put("imString03", (attrs.get("imString03").toString().replaceFirst("imString03: ", "")).trim());

                        String fullemp = (attrs.get("imString03").toString().replaceFirst("imString03: ", "")).trim();
                        if (fullemp.length() == 8) {
                            subID = fullemp.substring(2, 8);
                        }
                    } else {
                        map1.put("imString03", "");
                    }
                    map1.put("imString03sub", subID);

                    //4 Assign AD (imString52)
                    if (attrs.get("imString52") != null && !attrs.get("imString52").equals("")) {
                        map1.put("imString52", (attrs.get("imString52").toString().replaceFirst("imString52: ", "")).trim());
                    } else {
                        map1.put("imString52", "");
                    }

                    //5 Assign LDAP (imString53)
                    if (attrs.get("imString53") != null && !attrs.get("imString53").equals("")) {
                        map1.put("imString53", (attrs.get("imString53").toString().replaceFirst("imString53: ", "")).trim());
                    } else {
                        map1.put("imString53", "");
                    }

                    //6 Employee Status (imEmployeeStatus)
                    if (attrs.get("imEmployeeStatus") != null && !attrs.get("imEmployeeStatus").equals("")) {
                        map1.put("imEmployeeStatus", (attrs.get("imEmployeeStatus").toString().replaceFirst("imEmployeeStatus: ", "")).trim());
                    } else {
                        map1.put("imEmployeeStatus", "");
                    }

                    //7 Employee ID (employeeNumber)
                    if (attrs.get("employeeNumber") != null && !attrs.get("employeeNumber").equals("")) {
                        map1.put("employeeNumber", (attrs.get("employeeNumber").toString().replaceFirst("employeeNumber: ", "")).trim());
                    } else {
                        map1.put("employeeNumber", "");
                    }

                    //8 Enable
                    if (attrs.get("imEnabledState") != null && !attrs.get("imEnabledState").equals("")) {
                        map1.put("imEnabledState", (attrs.get("imEnabledState").toString().replaceFirst("imEnabledState: ", "")).trim());
                    } else {
                        map1.put("imEnabledState", "");
                    }

                    //9 Terminate Date (imExpirationDate)
                    if (attrs.get("imExpirationDate") != null && !attrs.get("imExpirationDate").equals("")) {
                        map1.put("imExpirationDate", (attrs.get("imExpirationDate").toString().replaceFirst("imExpirationDate: ", "")).trim());
                    } else {
                        map1.put("imExpirationDate", "");
                    }

                    userList.add(map1);
                }

            }
//            logger.info("[Step IDM 1.2] Enable users : " + userList.size());
            return userList;
        } catch (Exception e) {
//            logger.error("[Step IDM 1.2] Read Users Fail : " + e);
            System.err.println("IDM Read User error " + e);
            return null;
        }

    }

}
