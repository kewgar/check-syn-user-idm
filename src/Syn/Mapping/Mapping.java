package Syn.Mapping;

import static Syn.App_Controller_IDM.Controller_AD.Controller_AD;
import static Syn.App_Controller_IDM.Controller_IDM.Controller_IDM;
import static Syn.App_Controller_IDM.Controller_LDAP.Controller_LDPA;
import Syn.Endpoint.AD;
import static Syn.Endpoint.AD.connectAD;
import Syn.Endpoint.LDAP;
import static Syn.Endpoint.LDAP.connectLDAP;
import Syn.File.ReadProperties;
import Syn.IDM.IDM;
import static Syn.IDM.IDM.connectIDM;
import static Syn.Model.Model_Data.UserlistAD;
import static Syn.Model.Model_Data.UserlistIDM;
import static Syn.Model.Model_Data.UserlistLDAP;
import Syn.Runner.Runner;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.naming.NamingException;
import javax.naming.ldap.LdapContext;
import org.apache.log4j.Logger;

public class Mapping extends Syn.Model.Model_Data {

    public static final Logger logger = Logger.getLogger(Runner.class);

//    public static void main(String[] args) throws IOException, NamingException {
//        listOutput = new ArrayList<>();
//        listOutput = Mappinguser();
//        for (int i = 0; i < 5; i++) {
//            System.out.println((UserlistAD.get(i)));
//        }
//        
//    }
    public static ArrayList<HashMap<String, String>> Mappinguser(String app) throws IOException, NamingException {
//    public static void main(String[] args) throws IOException, NamingException {

//        ReadProperties.SetProvisioningInformation();
        HashMap<String, String> mapoutad = null;
        HashMap<String, String> mapoutldap = null;
        if (app.equalsIgnoreCase("AD")) {
            Controller_AD();
        }
        if (app.equalsIgnoreCase("LDAP")) {
            Controller_LDPA();
        }

        Controller_IDM();

        ArrayList<HashMap<String, String>> userList = new ArrayList<>();
        ADSynList = new ArrayList<>();
        LDAPSynList = new ArrayList<>();

        for (int i = 0; i < UserlistIDM.size(); i++) {

            if (app.equalsIgnoreCase("AD")) {
                //IDM TO AD
                for (int j = 0; j < UserlistAD.size(); j++) {
                    mapoutad = new HashMap<String, String>();
                    if (UserlistIDM.get(i).get("uid").equalsIgnoreCase(UserlistAD.get(j).get("eTSuborduid")) && UserlistIDM.get(i).get("imString52").equalsIgnoreCase("Yes")) {
                        //Users = synchro
                        //Attri AD
                        mapoutad.put("userid", (UserlistAD.get(j).get("eTSuborduid")));
                        mapoutad.put("userou", (UserlistAD.get(j).get("eTSubordou")));

                        //Attri IDM
                        mapoutad.put("uid", (UserlistIDM.get(i).get("uid")));
                        mapoutad.put("imEmployeeStatus", (UserlistIDM.get(i).get("imEmployeeStatus")));
                        mapoutad.put("imString03", (UserlistIDM.get(i).get("imString03")));
                        mapoutad.put("imString52", (UserlistIDM.get(i).get("imString52")));
                        mapoutad.put("imString53", (UserlistIDM.get(i).get("imString53")));
                        mapoutad.put("imLoginId", (UserlistIDM.get(i).get("imLoginId")));
                        mapoutad.put("imExpirationDate", (UserlistIDM.get(i).get("imExpirationDate")));
                        mapoutad.put("imEnabledState", (UserlistIDM.get(i).get("imEnabledState")));
                        mapoutad.put("Endpoint", "AD");
                        userList.add(mapoutad);
                        ADSynList.add(UserlistAD.get(j).get("eTSuborduid"));
//                    System.out.println("AD : " + mapoutad);
                        j = UserlistAD.size();

                    } else {
                        //User != synchro
                    }
                }
            }

            if (app.equalsIgnoreCase("LDAP")) {
                //IDM to LDAP
                for (int y = 0; y < UserlistLDAP.size(); y++) {
                    mapoutldap = new HashMap<String, String>();
//                System.out.println(UserlistIDM.get(i).get("employeeNumber") + " : " + (UserlistLDAP.get(y).get("eTSuborduid")));
//                System.err.println(UserlistIDM.get(i));
                    if (UserlistIDM.get(i).get("imString03sub").equalsIgnoreCase(UserlistLDAP.get(y).get("eTSuborduid")) && UserlistIDM.get(i).get("imString53").equalsIgnoreCase("Yes")) {
                        //Users = synchro
                        //Attri LDAP
                        mapoutldap.put("userid", (UserlistLDAP.get(y).get("eTSubordFulluid")));
                        mapoutldap.put("userou", (UserlistLDAP.get(y).get("eTSubordou")));

                        //Attri IDM
                        mapoutldap.put("uid", (UserlistIDM.get(i).get("uid")));
                        mapoutldap.put("imEmployeeStatus", (UserlistIDM.get(i).get("imEmployeeStatus")));
                        mapoutldap.put("imString03", (UserlistIDM.get(i).get("imString03")));
                        mapoutldap.put("imString52", (UserlistIDM.get(i).get("imString52")));
                        mapoutldap.put("imString53", (UserlistIDM.get(i).get("imString53")));
                        mapoutldap.put("imLoginId", (UserlistIDM.get(i).get("imLoginId")));
                        mapoutldap.put("imExpirationDate", (UserlistIDM.get(i).get("imExpirationDate")));
                        mapoutldap.put("imEnabledState", (UserlistIDM.get(i).get("imEnabledState")));
                        mapoutldap.put("Endpoint", "LDAP");
                        userList.add(mapoutldap);
                        LDAPSynList.add(UserlistLDAP.get(y).get("eTSubordFulluid"));
//                    System.out.println("LDAP : " + mapoutldap);
                        y = UserlistLDAP.size();
                    } else {
                        //user != synchro
                    }
                }
            }

        }
//        System.err.println(userList);
        return userList;
    }
}
