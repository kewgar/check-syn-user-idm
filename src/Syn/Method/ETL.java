package Syn.Method;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

public class ETL {

    public static void main(String[] args) {

        String b = "eTSubordinateClassEntry: eTDYNAccountName=K0123774,eTDYNContainerName=erm,eTDYNContainerName=people,eTDYNDirectoryName=IBMLDAP,eTNamespaceName=IBM JNDI,dc=im";
        String b2 = "eTSubordinateClassEntry: eTDYNAccountName=K0CA2481,eTDYNContainerName=ibb,eTDYNContainerName=people,eTDYNDirectoryName=IBMLDAP,eTNamespaceName=IBM JNDI,dc=im";

        String a = "K0abcdef";

        System.out.println(a.substring(2, a.length()));

    }

    public static String splittext(String text) {
//        System.err.println(text);
        try {
            if (text != null && !"".equals(text)) {

                String[] list = text.split(",");
                String uid = "";
                String ou = "";
                for (int i = 0; i < list.length; i++) {
                    String textCheck = text.split(",")[i];
                    //AD
                    if (textCheck.contains("eTADSAccountName")) {
                        uid = textCheck.replace("eTADSAccountName=", "").replace("eTSubordinateClassEntry: ", "");
                    }
                    if (textCheck.contains("eTADSOrgUnitName")) {
                        if (textCheck.contains("eTDYNContainerName=people")) {
                            ou = text.split(",")[i - 1].replace("eTADSOrgUnitName=", "").replace("eTSuperiorClassEntry: ", "");
                        }
                        ou = ou + ":" + text.split(",")[i].replace("eTADSOrgUnitName=", "").replace("eTSuperiorClassEntry: ", "");
                    }
                    if (textCheck.contains("eTADSContainerName")) {
                        ou = textCheck.replace("eTADSContainerName=", "").replace("eTSubordinateClassEntry: ", "");
                    }
                    //Global user
                    if (textCheck.contains("eTGlobalUserName")) {
                        uid = textCheck.replace("eTGlobalUserName=", "").replace("eTSuperiorClassEntry: ", "");
                    }
                    //LDAP
                    if (textCheck.contains("eTDYNAccountName")) {
                        uid = textCheck.replace("eTDYNAccountName=", "").replace("eTSubordinateClassEntry: ", "");
                    }
                    if (textCheck.contains("eTDYNContainerName")) {
                        if (textCheck.contains("eTDYNContainerName=people")) {
                            ou = text.split(",")[i - 1].replace("eTDYNContainerName=", "").replace("eTSubordinateClassEntry: ", "");
                        }
                        ou = ou + ":" + text.split(",")[i].replace("eTDYNContainerName=", "").replace("eTSubordinateClassEntry: ", "");
                    }
                }
                return (uid + "," + ou);
            } else {
                text = "NULL";
                return text;
            }
        } catch (Exception e) {
            System.err.println(e + text);
            return null + "fail";
        }

    }

    public static String changedateWhenCreate(String p) {
        String date = p.replace(".", "#").split("#")[0];
        String res = date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);
        return res;
    }

    public static String decode(String p) {
        byte[] decodedBytes = Base64.getDecoder().decode(p);
        String decodedString = new String(decodedBytes);

        return decodedString;
    }

    public static String changedateLDAP(String textDate) {

        if (textDate.matches("[0-9]{14}[.][0-9]{6}[Z]")) {
//            System.out.println("Get :" + textDate);
            String dateTimePart = textDate.substring(0, 18);
            String timeZonePart = "+" + textDate.substring(18, 21) + "0";
//            System.out.println("Split to :" + dateTimePart + " : " + timeZonePart);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss.SSSZ");
            Date theDate;
            try {
                theDate = sdf.parse(dateTimePart + timeZonePart);
//                System.out.println("Translated is :" + theDate);
//                System.out.println("Output is :" + new SimpleDateFormat("MM-dd-yyyy:HH.mm.ss").format(theDate));
                return new SimpleDateFormat("yyyy-MM-dd").format(theDate);
//                 return new SimpleDateFormat("MM-dd-yyyy:HH.mm.ss").format(theDate);
            } catch (ParseException ex) {
//                Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
//            System.err.println("Incorrect format :"+textDate +"\n"+"Accept only yyyyMMddHHmmss.SSSnnnZ format. \nExample 20181217043741.597000Z");
        }
        return null;

    }

    public static String stampdtcurent(String p) {
        String date = p.split(" ")[1];
        String month = p.split(" ")[2];
        String year = p.split(" ")[3];
        String Alldate = changeyear(year).concat("-" + changemonth(month) + "-" + changeday(date));

        return Alldate.trim();
    }

    public static String changemonth(String p) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("Jan", "01");
        map.put("Feb", "02");
        map.put("Mar", "03");
        map.put("Apr", "04");
        map.put("May", "05");
        map.put("Jun", "06");
        map.put("Jul", "07");
        map.put("Aug", "08");
        map.put("Sep", "09");
        map.put("Oct", "10");
        map.put("Nov", "11");
        map.put("Dec", "12");
        map.put("1", "01");
        map.put("2", "02");
        map.put("3", "03");
        map.put("4", "04");
        map.put("5", "05");
        map.put("6", "06");
        map.put("7", "07");
        map.put("8", "08");
        map.put("9", "09");
        map.put("10", "10");
        map.put("11", "11");
        map.put("12", "12");
        map.put("01", "01");
        map.put("02", "02");
        map.put("03", "03");
        map.put("04", "04");
        map.put("05", "05");
        map.put("06", "06");
        map.put("07", "07");
        map.put("08", "08");
        map.put("09", "09");

        String res = map.get(p);
        return res.trim();
    }

    public static String changeday(String p) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("1", "01");
        map.put("2", "02");
        map.put("3", "03");
        map.put("4", "04");
        map.put("5", "05");
        map.put("6", "06");
        map.put("7", "07");
        map.put("8", "08");
        map.put("9", "09");
        map.put("01", "01");
        map.put("02", "02");
        map.put("03", "03");
        map.put("04", "04");
        map.put("05", "05");
        map.put("06", "06");
        map.put("07", "07");
        map.put("08", "08");
        map.put("09", "09");

        for (int i = 10; i <= 31; i++) {
            String str1 = Integer.toString(i);
            String str2 = Integer.toString(i);
            map.put(str1, str2);
        }

        String res = map.get(p);
        return res.trim();
    }

    public static String changeyear(String p) {
        int years = Integer.parseInt(p);
        String res;
        if (years < 2000) {
            years = years + 417;
            res = String.valueOf(years);
        } else {
            return p;
        }
        return res;
    }

    public static String splittext2(String text) {
        System.err.println(text);
        try {
            if (text != null && !"".equals(text)) {
                if (text.contains("eTADSAccountName")) {
                    //Get UID AD
                    int startAD1 = text.indexOf("eTADSAccountName");
                    int endaAD1 = text.indexOf(",eTADSOrgUnitName");
                    String uidAD = text.substring(startAD1, endaAD1);
                    // Get OU AD
                    int startAD2 = text.indexOf("eTADSOrgUnitName");
                    int endAD2 = text.indexOf(",eTADSDirectoryName");
                    String ouAD = text.substring(startAD2, endAD2);

                    return uidAD + "#" + ouAD;
                } else if (text.contains("eTDYNContainerName")) {
                    // Get UID LDAP
                    int startLDAP1 = text.indexOf("eTDYNAccountName");
                    int endLDAP1 = text.indexOf(",");
                    String uidLDAP1 = text.substring(startLDAP1, endLDAP1);
                    // Get OU LDAP
                    int startLDAP2 = text.indexOf("eTDYNContainerName");
                    int endLDAP2 = text.indexOf(",eTDYNDirectoryName");
                    String ouLDAP2 = text.substring(startLDAP2, endLDAP2);

                    return uidLDAP1 + "#" + ouLDAP2;
                } else if (text.contains("eTADSContainerName")) {
                    //Get UID AD
                    int startAD3 = text.indexOf("eTADSAccountName");
                    int endaAD3 = text.indexOf(",eTADSContainerName");
                    String uidAD = text.substring(startAD3, endaAD3);
                    // Get OU AD
                    int startAD4 = text.indexOf("eTADSContainerName");
                    int endAD4 = text.indexOf(",");
                    String ouAD = text.substring(startAD4, endAD4);

                    return uidAD + "#" + ouAD;
                } else {
                    // Get GlOBAL USER
                    int startGlobal = text.indexOf("eTGlobalUserName=");
                    int endGlobal = text.indexOf(",");
                    String uidGlobal = text.substring(startGlobal, endGlobal);

                    return uidGlobal;
                }

            } else {
                text = "NULL";
                return text;
            }
        } catch (Exception e) {
            System.err.println(e + text);
            return null + "fail";
        }

    }

}
