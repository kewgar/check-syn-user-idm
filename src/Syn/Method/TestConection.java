/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Syn.Method;

import Syn.Model.Model_Data;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

/**
 *
 * @author 006284
 */
public class TestConection extends Model_Data{



    public static void Select_Server_Provisioning()
    {
        String server1 = (idm_server1.substring(0, idm_server1.length()-5)).split("//")[1];
        String server2 = idm_server2.substring(0, idm_server2.length()-5).split("//")[1];
        int port1 = Integer.parseInt(idm_server1.substring(idm_server1.length()-4, idm_server1.length()));
        int port2 =  Integer.parseInt(idm_server2.substring(idm_server2.length()-4, idm_server2.length()));
        
        if(isSocketAliveUitlitybyCrunchify(server2,port2))
        {
            PROVIDER_URL_IDM = PROVIDER_URL_IDM_2;
            server = idm_server2;
            selected_server = "2";
            System.err.println("Server2 have been select.");
//            logger.info("Server2 have been select.");

        }
        else if(isSocketAliveUitlitybyCrunchify(server1,port1))
        {
            PROVIDER_URL_IDM = PROVIDER_URL_IDM_1;
            server = idm_server1;
            selected_server = "1";
            System.err.println("Server1 have been select.");
//            logger.info("Server1 have been select.");
        }
        else
        {
            System.err.println("Can't connect with all server.");
//            logger.info("Can't connect with all server.");
        }
        
        log(server +" is number "+selected_server);
                
    }
   
    public static boolean isSocketAliveUitlitybyCrunchify(String hostName, int port) {
        boolean isAlive = false;

        // Creates a socket address from a hostname and a port number
        SocketAddress socketAddress = new InetSocketAddress(hostName, port);
        Socket socket = new Socket();

        // Timeout required - it's in milliseconds
        int timeout = 2000;

        log("hostName: " + hostName + ", port: " + port);
        try {
            socket.connect(socketAddress, timeout);
            socket.close();
            isAlive = true;

        } catch (SocketTimeoutException exception) {
            System.out.println("SocketTimeoutException " + hostName + ":" + port + ". " + exception.getMessage());
        } catch (IOException exception) {
            System.out.println(
                    "IOException - Unable to connect to " + hostName + ":" + port + ". " + exception.getMessage());
        }
        return isAlive;
    }

    // Simple log utility
    private static void log(String string) {
        System.out.println(string);
//        logger.info("connected "+string);
    }


}
