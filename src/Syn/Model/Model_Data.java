/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Syn.Model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Model_Data {

    //ArrayList
    public static ArrayList<HashMap<String, String>> Totaluser, ResultTotaluser, UserlistAD, reSignList, UserlistLDAP, UserlistIDM, listOutput;
    public static ArrayList<String> UserlistDB, currentUIDList,ADSynList,LDAPSynList;

    //String All
    public static String INITIAL_CONTEXT_FACTORY_all;
    public static String SECURITY_AUTHENTICATION_all;
    public static String server;
    public static String selected_server;

    //IDM
    public static String SECURITY_PRINCIPAL_IDM;
    public static String SECURITY_CREDENTIALS_IDM;
    public static String PROVIDER_URL_IDM;
    public static String PROVIDER_URL_IDM_1;
    public static String PROVIDER_URL_IDM_2;
    public static String searchFilter_IDM;
    public static String searchBase_IDM;
    //Attri IDM
    public static String[] returnedAtts_IDM
            = {"uid", "imEmployeeStatus", "employeeNumber", "imString03", "imString52", "imString53", "imLoginId", "imExpirationDate", "imEnabledState"};

    //AD
    public static String SECURITY_PRINCIPAL_AD;
    public static String SECURITY_CREDENTIALS_AD;
    public static String PROVIDER_URL_AD;
    public static String searchFilter_AD;
    public static String searchBase_AD;
    //Attri AD
    public static String[] returnedAtts_AD = {"eTSubordinateClassEntry", "eTSuperiorClassEntry" ,"eTCID"};

    //LDAP
    public static String SECURITY_PRINCIPAL_LDAP;
    public static String SECURITY_CREDENTIALS_LDAP;
    public static String PROVIDER_URL_LDAP;
    public static String searchFilter_LDAP;
    public static String searchBase_LDAP;
    //Attri LDAP
    public static String[] returnedAtts_LDAP = {"eTSubordinateClassEntry", "eTSuperiorClassEntry","eTCID","*"};

    //path
    public static String log_path;
    public static String output_path;
    public static String idm_server1;
    public static String idm_server2;
    public static String idm_enviroment;

    //Con DB
    public static String CONNECTION_STRING;
}
