package Syn.Runner;

import static Syn.File.Output.Controller_map;
import static Syn.File.Output.writeCSVfile;
import Syn.File.ReadProperties;
import Syn.Mapping.Mapping;
import static Syn.Method.ETL.stampdtcurent;
import Syn.Method.TestConection;
import static Syn.Model.Model_Data.*;
import static Syn.Model.Model_Data.listOutput;
import static Syn.Model.Model_Data.output_path;
import static Syn.Model.Model_Data.selected_server;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.naming.NamingException;

public class Runner {

//    public static void main(String[] args) throws IOException, NamingException {
//
//        ReadProperties.SetProvisioningInformation();
//        TestConection.Select_Server_Provisioning();
//        Controller_map();
//        writeCSVfile(output_path + selected_server + "SynUser_" + stampdtcurent(ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME)) + ".csv");
//
//    }
    public static void main(String[] args) throws IOException, NamingException {
        System.out.println(GetADSyn("D:\\KBTG\\code\\CE-IDM_CheckUserSyn\\CE-IDM_CheckUser.properties"));
        System.out.println(GetLDAPSyn("D:\\KBTG\\code\\CE-IDM_CheckUserSyn\\CE-IDM_CheckUser.properties"));
    }

    public static ArrayList<String> GetADSyn(String path) throws IOException, NamingException {

        ReadProperties.SetProvisioningInformation(path);
        TestConection.Select_Server_Provisioning();
        Controller_map("AD");
        return ADSynList;
    }

    public static ArrayList<String> GetLDAPSyn(String path) throws IOException, NamingException {

        ReadProperties.SetProvisioningInformation(path);
        TestConection.Select_Server_Provisioning();
        Controller_map("LDAP");
        return LDAPSynList;
    }
}
